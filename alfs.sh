#!/bin/bash
# This script has two main part:
#
#  1. add user 'lfs', create a disk image and mount it
#  2. build the LFS system
#
# The former is done if the script is run as root, the latter for any other user.
# The first one needs a filename and the second one needs the mount point where this image is mounted.

# Very complex argument management
[ -z "$1" -a $(id -u) -eq 0 ] && { echo -e "\nUsage: $0 <disk image name>\n"; exit 1; }
[ -z "$1" ] && { echo -e "\nUsage: $0 <image mountpoint>\n"; exit 1; }

# Original and shiny variables
ALFS_DISK="/home/lfs/$1"
ALFS_BIN=/home/lfs/bin
ALFS_HOME=/home/lfs
ALFS_LOG=/home/lfs/logs
ALFS_MNT=/home/lfs/target

## DISK IMAGE CREATION AND MOUNT ##
[ $(id -u) -eq 0 ] && {
    echo " ** Running as root. Setting up the environement **"
    adduser --gecos 'Another Linux From Scratch' --quiet --home "$ALFS_HOME" --shell /bin/bash --disabled-password lfs
    # New user ?
    if [ $? -eq 0 ]; then
        # Get him a virgin shell
        cat > "$ALFS_HOME"/.bash_profile << "EOF"
        exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF
        cat > "$ALFS_HOME"/.bashrc << "EOF"
        set +h
        umask 022
        LFS="$ALFS_MNT"
        LC_ALL=POSIX
        LFS_TGT=$(uname -m)-lfs-linux-gnu
        PATH=/tools/bin:/bin:/usr/bin
        export LFS LC_ALL LFS_TGT PATH
EOF
    fi
    # Install some packages on host.
    apt-get install bison flex subversion git

    # Image exists ?
    if [ ! -f "$ALFS_DISK" ]; then
        # Create the image
        qemu-img create -f raw "$ALFS_DISK" 16G && chown lfs:lfs "$ALFS_DISK"
        read vvff
        echo -e "n\np\n1\n\n\nw" | fdisk "$ALFS_DISK"
        read bbgg
        ALFS_FS=$(losetup --partscan --show --find "$ALFS_DISK")
        read hhyy
        mkfs.ext4 -L LFS -m 1 ${ALFS_FS}p1
    fi
    # Loop device OK ? Else set it.
    [ -n "${ALFS_FS}" ] || ALFS_FS=$(losetup --partscan --show --find "$ALFS_DISK")
    # Mount the image.
    mkdir "$ALFS_BIN" "$ALFS_MNT"
    cp "$0" "$ALFS_BIN/$(basename $0)"
    chown -R lfs:lfs "$ALFS_HOME" "$ALFS_MNT"
    chmod +x "$ALFS_HOME"/bin/*    
    [ -n "$ALFS_FS" ] || { echo " ! Can’t find a partition on $ALFS_DISK."; exit 2; }
    mount | grep -q " $ALFS_MNT " || { 
        mount -o noatime,rw,user=lfs ${ALFS_FS}p1 "$ALFS_MNT" || { echo " ! Can’t mount ${ALFS_FS}p1 on $ALFS_MNT."; exit 3; }
        chown lfs:lfs "$ALFS_MNT"
    }
    [ -h "/tools" ]  || cd / && ln -sv $ALFS_HOME/tools
    df -Ph "$ALFS_MNT" |tail -n +2
    echo -e "\n * Everything’s OK. Run me now as user 'lfs' with target '$ALFS_MNT' as first argument.\n" && exit 0;
}

## ALFS BUILD ##
## We’re not root… Let’s go !
# First argument is our target directory
ALFS_MNT="$1"
# Ensure the LFS variable is defined, even if lfs’s .bash_profile should have valued it already
LFS="$(readlink -f "$ALFS_MNT")"
echo " ** \$LFS=$LFS **"

# Check if the filesystem is mounted and cd to it.
mount | grep -vq " $ALFS_MNT " || { echo " ! $ALFS_MNT not mounted."; exit 4; }
cd "$ALFS_HOME"

# Create some directories if they don’t exist.
[ -d "$ALFS_HOME" ] || mkdir -p "$ALFS_HOME"
[ -d "$ALFS_LOG" ]  || mkdir -p "$ALFS_LOG"
[ -d "$ALFS_BIN" ]  || mkdir -p "$ALFS_BIN"
[ -d "$ALFS_HOME/sources" ] || mkdir $ALFS_HOME/sources
[ -d "$ALFS_HOME/tools" ]   || mkdir $ALFS_HOME/tools

prepare() {
    case $(uname -m) in
    x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
    esac
}

get_sources() {
    # get_sources(url,tag/branch/commit)
    cd "$ALFS_HOME/sources"
    dirname=$(echo ${1##*/} | cut -d'.' -f1)
    if [ ! -d "$dirname" ]; then
        git clone ${1}
        git checkout ${2}
    else
        cd "$dirname"
        git pull origin master
        git checkout ${2}
    fi
    cd "$ALFS_HOME/sources"
}

declare -a ALFS_SOURCE

ALFS_SOURCE["binutils-gdb"]='git://sourceware.org/git/binutils-gdb.git master'

binutils_0() {
    # Binutils (pass 1)
    get_sources ${ALFS_SOURCE["binutils-gdb"]%% *} ${ALFS_SOURCE["binutils-gdb"]##* }
    cd binutils-gdb
    rm -rf build && mkdir -v build
    cd build
    ../configure --prefix=/tools            \
                 --with-sysroot=$LFS        \
                 --with-lib-path=/tools/lib \
                 --target=$LFS_TGT          \
                 --disable-nls              \
                 --disable-werror         &&\
    make && make install && make clean
    cd ..
}


## MAIN ##

prepare
binutils_0


exit 0


#~ # GCC (step 1)

#~ cd "$ALFS_HOME/sources"
#~ mkdir GCC
#~ svn checkout svn://gcc.gnu.org/svn/gcc/trunk GCC
#~ cd GCC
#~ ./contrib/download_prerequisites

#~ read xxxxx

#~ rm -rf build
#~ mkdir -v build && cd build
    
#~ ../configure                                       \
    #~ --target=$LFS_TGT                              \
    #~ --prefix=/tools                                \
    #~ --with-glibc-version=2.25                      \
    #~ --with-sysroot=$LFS                            \
    #~ --with-newlib                                  \
    #~ --without-headers                              \
    #~ --with-local-prefix=/tools                     \
    #~ --with-native-system-header-dir=/tools/include \
    #~ --disable-nls                                  \
    #~ --disable-shared                               \
    #~ --disable-multilib                             \
    #~ --disable-decimal-float                        \
    #~ --disable-threads                              \
    #~ --disable-libatomic                            \
    #~ --disable-libgomp                              \
    #~ --disable-libmpx                               \
    #~ --disable-libquadmath                          \
    #~ --disable-libssp                               \
    #~ --disable-libvtv                               \
    #~ --disable-libstdcxx                            \
    #~ --enable-languages=c,c++

#~ read xxxxx

#~ make 

#~ read xxxxx

#~ make install


#~ exit 0



#~ # Linux API headers

#~ # cd "$ALFS_HOME/sources"
#~ # git clone https://github.com/torvalds/linux.git
#~ # cd linux
#~ cd "$ALFS_HOME/sources/linux"
#~ git pull origin master
#~ git checkout v4.14
#~ make mrproper
#~ make INSTALL_HDR_PATH=dest headers_install
#~ cp -rv dest/include/* /tools/include

#~ exit 0

# glibc

#~ # cd "$ALFS_HOME/sources"
#~ # git clone git://sourceware.org/git/glibc.git
#~ # cd glibc
#~ cd "$ALFS_HOME/sources/glibc"
#~ git reset --hard HEAD
#~ git pull origin master
#~ git checkout glibc-2.26.9000
#~ rm -rf build
#~ mkdir -v build && cd build

#~ ../configure                             \
      #~ --prefix=/tools                    \
      #~ --host=$LFS_TGT                    \
      #~ --target=$LFS_TGT                  \
      #~ --build=$(../scripts/config.guess) \
      #~ --enable-kernel=3.2                \
      #~ --with-headers=/tools/include      \
      #~ libc_cv_forced_unwind=yes          \
      #~ libc_cv_c_cleanup=yes

#~ make
#~ make install

#~ exit 0

#~ ## !! This is stupid:
#~ cp -r "$ALFS_HOME"/tools/lib/* "$ALFS_HOME"/tools/lib/gcc/x86_64-lfs-linux-gnu/7.2.0
    
## Check if the build is OK:

#~ echo 'int main(){}' > dummy.c
#~ $LFS_TGT-gcc dummy.c
#~ readelf -l a.out | grep ': /tools'

## Should output: `[Requesting program interpreter: /tools/lib/ld-linux.so.2]`

#~ exit 0


#~ ## libstdc++

#~ cd "$ALFS_HOME/sources/GCC"

#~ rm -rf build-libstdc++
#~ mkdir -v build-libstdc++
#~ cd build-libstdc++
#~ CPPFLAGS="-I/tools/include -I/opt/linux-headers-sabotage/usr/include" 
#~ ../libstdc++-v3/configure           \
    #~ --host=$LFS_TGT                 \
    #~ --prefix=/tools                 \
    #~ --disable-multilib              \
    #~ --disable-nls                   \
    #~ --disable-libstdcxx-threads     \
    #~ --disable-libstdcxx-pch         \
    #~ --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/7.2.0
    
#~ make
#~ make install




#~ exit 0

#~ # Binutils (step 2)

#~ cd "$ALFS_HOME"/sources/binutils-gdb

#~ cd build && make clean

#~ #--host=$LFS_TGT            
#~ CC=$LFS_TGT-gcc                \
#~ AR=$LFS_TGT-ar                 \
#~ CPPFLAGS=-I/tools/include      \
#~ RANLIB=$LFS_TGT-ranlib         \
#~ ../configure                   \
    #~ --prefix=/tools            \
    #~ --disable-nls              \
    #~ --disable-werror           \
    #~ --with-lib-path=/tools/lib \
    #~ --with-sysroot             \
    #~ --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/7.2.0

#~ make
#~ read tt
#~ make install
#~ make -C ld clean
#~ make -C ld LIB_PATH=/usr/lib:/lib
#~ cp -v ld/ld-new /tools/bin
    

#~ exit 0
